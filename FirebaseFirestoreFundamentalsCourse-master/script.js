$(document).ready(function () {
    //get all the data on app startup
    $('#createQuestion').click(function () {
        $('.questionForm').css("display", "block");
        $('#dynamicBtn').text('Guardar Cambios')
    });

    $('#dynamicBtn').click(function () {
        //pregunta form values
        var pregunta = $("#pregunta").val();
        var respuesta = $("#respuesta").val();
        var puntos = $("#puntos").val();
        var age = $("#age").val();
        var gender = $("#gender").val();
        var yearsOfExperience = $("#yearsOfExperience").val();
        var isfulltime = $('#isFullTime').is(":checked")

        //check if you need to create or update an pregunta
        if ($(this).text() == "Guardar Cambios") {} else {}
    });

    // Cancel the Pregunta form
    $('#cancel').click(function () {
        $('.questionForm').css("display", "none");
    });

    // Get the data of the pregunta you want to edit
    $("tbody.tbodyData").on("click", "td.editQuestion", function () {
        $('.questionForm').css("display", "block");
        $('#dynamicBtn').text('Update Pregunta');

        $("#pregunta").val($(this).closest('tr').find('.pregunta').text());
        $("#respuesta").val($(this).closest('tr').find('.respuesta').text());
        $("#puntos").val($(this).closest('tr').find('.puntos').text());
        $("#age").val($(this).closest('tr').find('.age').text());
        $("#gender").val($(this).closest('tr').find('.gender').text());
        $("#yearsOfExperience").val($(this).closest('tr').find('.yearsofexperience').text());
        $("#isFullTime").prop('checked', $(this).closest('tr').find('.isfulltime').text() === 'true');
    });

    // Delete pregunta
    $("tbody.tbodyData").on("click", "td.deleteQuestion", function () {
        //Get the Pregunta Data
        var pregunta = $(this).closest('tr').find('.pregunta').text(); //Pregunta
        var respuesta = $(this).closest('tr').find('.respuesta').text(); //Respuesta
    });

    $("#searchPregunta").change(function () {
        console.log('You entered: ', $(this).val());
    });
});